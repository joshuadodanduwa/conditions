//creating input for what game to play
var userInput = ""


//allowing user to input game
userInput = prompt("Hi there. You have two games to play; guess my number or what is my favourite pet.\nType Number for guess my number, or type Pet for what is my favourite pet.")
//making letters lowercase
userInput = userInput.toLowerCase()

//guess my number game
if (userInput === "number") {

    //Creating the correct number and number input
    var userNum = 0
    var myNum = 7
   
    //creating while loop
    while(userNum !== 7){
        
        //allowing user to input number    
        userNum = Number(prompt("My number is between 1 and 10. Guess my number and type in the box:"))
        
        //checking the inputted number
        if(userNum === myNum) {
            console.log ("You guessed correctly!")
        
        }
        else if(userNum < myNum) {
        
            console.log("too low")
        }
        else {
        
            console.log("too high")
        }
    }

}

//guess my favourite pet game
else if (userInput === "pet"){

    //creating my favourite imaginary pet and pet input 
    var petInput = ""

    //creating while loop
    while(petInput !== "dog") {
            
        //allowing user to input a pet
        petInput = prompt("I have four imaginary pets: Cat, Dog, Fish and Rabbit.\nGuess my favourite:")

        //making letters lowercase
        petInput = petInput.toLowerCase()


        //switch statements

        switch(petInput) {

            case "cat":
                console.log("My cat is not my favourite!")
                break

            case "dog":
                console.log("My dog is my favourite!")
                break

            case "fish":
                console.log("My fish is not my favourite!")
                break
            
            case "rabbit":
                console.log("My rabbit is not my favourite!")
                break

            default:
                console.log("Not a correct input. Try again.")
                break

            }

    }
}

//creating a message for an invalid input
else {
    alert("Invalid input. You did not specify one of the games.")
}

